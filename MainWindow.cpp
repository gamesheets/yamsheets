#include "MainWindow.h"
#include "ui_MainWindow.h"
#include "PlayerSheet.h"
#include "PlayersManager.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , _playerManager(nullptr)
{
    ui->setupUi(this);
    connect(ui->actionRestart, &QAction::triggered, this , &MainWindow::restart);
    connect(ui->actionPlayers, &QAction::triggered, this, &MainWindow::managePlayers);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::addPlayer(const QString & name)
{
    PlayerSheet *playerSheet = new PlayerSheet(this);
    playerSheet->setName(name);
    ui->centralwidget->layout()->addWidget(playerSheet);

    _sheets.insert(name,playerSheet);
}

void MainWindow::restart()
{
    for(auto sheet : _sheets)
    {
        sheet->restart();
    }
}

void MainWindow::managePlayers()
{
    if(!_playerManager)
        _playerManager = new PlayersManager();
    connect(_playerManager, &PlayersManager::playersChanged, this, &MainWindow::updatePlayers);
    _playerManager->show();
}

void MainWindow::updatePlayers(const QStringList &players)
{
    qDeleteAll(_sheets);
    _sheets.clear();

    for(QString player : players)
    {
        addPlayer(player);
    }
    _playerManager->hide();
}


