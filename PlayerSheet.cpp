#include "PlayerSheet.h"
#include "ui_PlayerSheet.h"

PlayerSheet::PlayerSheet(QWidget *parent) :
    QWidget(parent),
    _name(QString()),
    ui(new Ui::PlayerSheet),
    _totalScore(0),
    _difference(0),
    _totalScoreNumber(0),
    _hasBonus(false),
    _hasSuite(false),
    _hasFull(false),
    _hasSquare(false),
    _hasYam(false)
{
    ui->setupUi(this);

    //On connecte les boutons de l'IHM
    for(const auto& widget : {ui->sb1,ui->sb2,ui->sb3,ui->sb4,ui->sb5,ui->sb6})
    {
        connect(widget,&QSpinBox::valueChanged, this, &PlayerSheet::updateScoreNumber);
        connect(widget,&QSpinBox::valueChanged, this, &PlayerSheet::onEditingFinished);
    }

    for(const auto& widget : {ui->sbMax,ui->sbMin})
    {
        connect(widget,&QSpinBox::valueChanged, this, &PlayerSheet::updateDifference);
        connect(widget,&QSpinBox::valueChanged, this, &PlayerSheet::onEditingFinished);
    }

    for(const auto& widget : {ui->bbSuite,ui->bbFull, ui->bbSquare, ui->bbYam})
    {
        connect(widget,&QDialogButtonBox::accepted, this, &PlayerSheet::onFigureClicked);
        connect(widget,&QDialogButtonBox::clicked, this, &PlayerSheet::onEditingFinished);
    }
}

PlayerSheet::~PlayerSheet()
{
    delete ui;
}

void PlayerSheet::setName(const QString &name)
{
    _name = name;
    ui->lblPlayer->setText(_name);
    emit nameChanged(_name);
}

void PlayerSheet::restart()
{
    for(const auto& widget : {ui->sb1,ui->sb2,ui->sb3,ui->sb4,ui->sb5,ui->sb6})
    {
        widget->setValue(0);
        widget->setEnabled(true);
    }
    for(const auto& widget : {ui->sbMin,ui->sbMax})
    {
        widget->setValue(5);
        widget->setEnabled(true);
    }
    for(const auto& widget : {ui->bbFull,ui->bbSquare,ui->bbSuite,ui->bbYam})
    {
        widget->setEnabled(true);
    }
    restartFigures();
}

void PlayerSheet::restartFigures()
{
    for(auto b : {&_hasBonus,&_hasFull,&_hasSquare,&_hasSuite, &_hasYam})
    {
        *b = false;
    }
    updateScoreTotalFigure();
}

void PlayerSheet::updateScoreNumber()
{
    _totalScoreNumber = 0;
    _totalScoreNumber+= ui->sb1->value();
    _totalScoreNumber+= ui->sb2->value()*2;
    _totalScoreNumber+= ui->sb3->value()*3;
    _totalScoreNumber+= ui->sb4->value()*4;
    _totalScoreNumber+= ui->sb5->value()*5;
    _totalScoreNumber+= ui->sb6->value()*6;

    //On vérifie si on a le bonus pour metre à jour
    updateBonus();

    //Mise à jour des élément de l'IHM
    ui->lblTotalSumm->setText(QString::number(_totalScoreNumber));
    updateScoreTotalNumber();
}

void PlayerSheet::updateDifference()
{
    _difference = ui->sbMax->value() - ui->sbMin->value();

    //Mise à jour de l'IHM
    ui->lblDiference->setText(QString::number(_difference));
    updateTotalScore();
}

void PlayerSheet::updateTotalScore()
{
    _totalScore = _totalScoreNumber;
    if(_hasBonus)
    {
        _totalScore +=30;
    }
    _totalScore+=_difference;
    if(hasSuite())
    {
        _totalScore+=20;
    }
    if(hasFull())
    {
        _totalScore+=30;
    }
    if(hasSquare())
    {
        _totalScore+=40;
    }
    if(hasYam())
    {
        _totalScore+=50;
    }

    ui->lblTotalScore->setText(QString::number(_totalScore));

    emit totalScoreChanged(_totalScore);
}

void PlayerSheet::updateScoreTotalFigure()
{
    int score = 0;
    if(_hasSquare)
        score += 40;
    if(_hasSuite)
        score += 20;
    if(_hasFull)
        score += 30;
    if(_hasYam)
        score += 50;
    ui->lblScoreFigures->setText(QString::number(score));
    updateTotalScore();
}

void PlayerSheet::updateScoreTotalNumber()
{
    int score = _totalScoreNumber;
    if(_hasBonus)
        score += 30;
    ui->lblScoreTotalNb->setText(QString::number(score));
    updateTotalScore();
}

void PlayerSheet::updateBonus()
{
    _hasBonus = _totalScoreNumber >= 60;
    if(_hasBonus)
    {
        //le bonus est de 30 points
        ui->lblBonus->setText(QString::number(30));
    }
    else
    {
        ui->lblBonus->clear();
    }
}

bool PlayerSheet::hasSuite() const
{
    return _hasSuite;
}

bool PlayerSheet::hasFull() const
{
    return _hasFull;
}

bool PlayerSheet::hasSquare() const
{
    return _hasSquare;
}

bool PlayerSheet::hasYam() const
{
    return _hasYam;
}

void PlayerSheet::onEditingFinished()
{
    QObject* obj = sender();
    QWidget * w = dynamic_cast<QWidget*>(obj);
    if(w)
    {
        w->setEnabled(false);
    }
}

void PlayerSheet::onFigureClicked()
{
    QObject* obj = sender();
    if(obj == ui->bbSuite)
    {
        _hasSuite = true;
    }
    else if(obj == ui->bbFull)
    {
        _hasFull = true;
    }
    else if(obj == ui->bbSquare)
    {
        _hasSquare = true;
    }
    else if(obj == ui->bbYam)
    {
        _hasYam = true;
    }

    //On met à jour l'IHM
    updateScoreTotalFigure();

    //TODO mettre en visible le bouton cliqué
}

