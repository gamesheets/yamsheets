#ifndef DICENUMBERSSHEETS_H
#define DICENUMBERSSHEETS_H

#include <QWidget>
#include <QVector>

namespace Ui {
class DiceNumbersSheets;
}

class DiceNumberWidget;

class DiceNumbersSheets : public QWidget
{
    Q_OBJECT

public:
    explicit DiceNumbersSheets(QWidget *parent = nullptr);
    ~DiceNumbersSheets();
    void setSize(int size);

    void updateScore();

private:
    Ui::DiceNumbersSheets *ui;
    QVector<DiceNumberWidget*> _widgets;
};

#endif // DICENUMBERSSHEETS_H
