#include "DiceNumberWidget.h"
#include "ui_DiceNumberWidget.h"

DiceNumberWidget::DiceNumberWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DiceNumberWidget)
{
    ui->setupUi(this);
    connect(ui->pbEdit, &QPushButton::clicked, this, &DiceNumberWidget::editNumber);
    connect(ui->sbNumber, &QSpinBox::valueChanged, this, &DiceNumberWidget::onNumberSelected);
}

DiceNumberWidget::~DiceNumberWidget()
{
    delete ui;
}

void DiceNumberWidget::editNumber()
{
    ui->sbNumber->setEnabled(true);
    ui->sbNumber->setFocus();
}

void DiceNumberWidget::onNumberSelected()
{
    ui->sbNumber->setEnabled(false);
    emit numberChanged();
}

int DiceNumberWidget::getNumber() const
{
    return ui->sbNumber->value();
}
