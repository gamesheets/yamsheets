#ifndef PLAYERSHEET_H
#define PLAYERSHEET_H

#include <QWidget>

namespace Ui {
class PlayerSheet;
}

class PlayerSheet : public QWidget
{
    Q_OBJECT

public:
    explicit PlayerSheet(QWidget *parent = nullptr);
    ~PlayerSheet();

    void setName(const QString & name);

    int getTotalScore() const
    {
        return _totalScore;
    };

    void restart();

private:
    //Fonction associé au score
    void updateScoreNumber();
    void updateDifference();
    void updateTotalScore();
    void updateScoreTotalFigure();
    void updateScoreTotalNumber();
    void updateBonus();
    bool hasSuite() const;
    bool hasFull() const;
    bool hasSquare() const;
    bool hasYam() const;

    void onEditingFinished();
    void onFigureClicked();

    void restartFigures();
signals:
    void nameChanged(const QString& name) const;
    void totalScoreChanged(int score) const;
    void sheetCompleted() const;
private:
    QString _name;
    Ui::PlayerSheet *ui;

    int _totalScore;
    int _difference;
    int _totalScoreNumber;
    bool _hasBonus;
    bool _hasSuite;
    bool _hasFull;
    bool _hasSquare;
    bool _hasYam;
};

#endif // PLAYERSHEET_H
