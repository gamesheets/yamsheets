#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QHash>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class PlayerSheet;
class PlayersManager;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void addPlayer(const QString & name);
    void restart();
    void managePlayers();
    void updatePlayers(const QStringList & players);
private:
    Ui::MainWindow *ui;
    PlayersManager *_playerManager;
    QHash<QString, PlayerSheet*> _sheets;

};
#endif // MAINWINDOW_H
