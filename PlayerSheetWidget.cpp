#include "PlayerSheetWidget.h"
#include "ui_PlayerSheetWidget.h"

PlayerSheetWidget::PlayerSheetWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PlayerSheetWidget)
{
    ui->setupUi(this);
}

PlayerSheetWidget::~PlayerSheetWidget()
{
    delete ui;
}
