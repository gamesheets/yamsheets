QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    AddPlayer.cpp \
    DiceNumberWidget.cpp \
    DiceNumbersSheets.cpp \
    PlayerSheet.cpp \
    PlayerSheetWidget.cpp \
    PlayersManager.cpp \
    main.cpp \
    MainWindow.cpp

HEADERS += \
    AddPlayer.h \
    DiceNumberWidget.h \
    DiceNumbersSheets.h \
    MainWindow.h \
    PlayerSheet.h \
    PlayerSheetWidget.h \
    PlayersManager.h

FORMS += \
    DiceNumberWidget.ui \
    DiceNumbersSheets.ui \
    MainWindow.ui \
    PlayerSheet.ui \
    PlayerSheetWidget.ui \
    PlayersManager.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
