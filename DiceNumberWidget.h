#ifndef DICENUMBERWIDGET_H
#define DICENUMBERWIDGET_H

#include <QWidget>

namespace Ui {
class DiceNumberWidget;
}

class DiceNumberWidget : public QWidget
{
    Q_OBJECT

public:
    explicit DiceNumberWidget(QWidget *parent = nullptr);
    ~DiceNumberWidget();

    void editNumber();
    void onNumberSelected();
    int getNumber() const;

    signals:
    void numberChanged() const;
private:
    Ui::DiceNumberWidget *ui;
};

#endif // DICENUMBERWIDGET_H
