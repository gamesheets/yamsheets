#include "DiceNumbersSheets.h"
#include "ui_DiceNumbersSheets.h"
#include "DiceNumberWidget.h"

DiceNumbersSheets::DiceNumbersSheets(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DiceNumbersSheets)
{
    ui->setupUi(this);
}

DiceNumbersSheets::~DiceNumbersSheets()
{
    delete ui;
}

void DiceNumbersSheets::setSize(int size)
{
    qDeleteAll(_widgets);
    _widgets.clear();

    _widgets.resize(size);
    for(int i = 0 ; i < size ; ++i)
    {
        DiceNumberWidget* widget = new DiceNumberWidget(this);
        ui->verticalLayout->insertWidget(i,widget);
        _widgets[i] = widget;
        connect(widget, &DiceNumberWidget::numberChanged, this, &DiceNumbersSheets::updateScore);
    }
}

void DiceNumbersSheets::updateScore()
{
    int score = 0;
    for(int i = 0 ; i <  _widgets.size() ; ++i)
    {
         score += _widgets[i]->getNumber() * (i+1); // On fait la somme en faisant le nombre de dés * valeur du dé
    }
    //On met à jour les IHMs
    ui->lblSum->setText(QString::number(score));
    int bonus = 0;
    if(score>=60)
        bonus = 30;

    ui->lblBonus->setText(QString::number(bonus));
    ui->lblTotal->setText(QString::number(score+bonus));
}
