#ifndef PLAYERSHEETWIDGET_H
#define PLAYERSHEETWIDGET_H

#include <QWidget>

namespace Ui {
class PlayerSheetWidget;
}

class PlayerSheetWidget : public QWidget
{
    Q_OBJECT

public:
    explicit PlayerSheetWidget(QWidget *parent = nullptr);
    ~PlayerSheetWidget();

private:
    Ui::PlayerSheetWidget *ui;
};

#endif // PLAYERSHEETWIDGET_H
