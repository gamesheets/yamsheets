#ifndef PLAYERSMANAGER_H
#define PLAYERSMANAGER_H

#include <QWidget>
#include <QList>

class QLineEdit;

namespace Ui {
class PlayersManager;
}

class PlayersManager : public QWidget
{
    Q_OBJECT

public:
    explicit PlayersManager(QWidget *parent = nullptr);
    ~PlayersManager();

    void addPlayer();
    void removePlayer();
    void createPlayers();

signals:
    void playersChanged(const QStringList& players);

private:
    Ui::PlayersManager *ui;
    unsigned int _playerId;
    QList<QLineEdit*> _playersNames;
};

#endif // PLAYERSMANAGER_H
