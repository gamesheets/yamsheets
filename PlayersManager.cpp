#include "PlayersManager.h"
#include "ui_PlayersManager.h"
#include <QLineEdit>


PlayersManager::PlayersManager(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PlayersManager),
    _playerId(1),
    _playersNames(QList<QLineEdit*>())
{
    ui->setupUi(this);
    connect(ui->pbAddPlayer, &QPushButton::clicked, this, &PlayersManager::addPlayer);
    connect(ui->pbValidate, &QPushButton::clicked, this, &PlayersManager::createPlayers);
    connect(ui->pbDeletePlayer, &QPushButton::clicked, this, &PlayersManager::removePlayer);
}

PlayersManager::~PlayersManager()
{
    delete ui;
}

void PlayersManager::addPlayer()
{
    QLineEdit * lineEdit = new QLineEdit();
    lineEdit->setText(QStringLiteral("Player") + QString::number(_playerId++));
    ui->playersArea->layout()->addWidget(lineEdit);
    _playersNames.append(lineEdit);
}

void PlayersManager::removePlayer()
{
    if(_playersNames.size()>0)
    {
        QLineEdit* lastWidget = _playersNames.takeLast();
        delete lastWidget;
    }
}

void PlayersManager::createPlayers()
{
    QStringList players;
    for(QLineEdit * lineEdit : _playersNames)
    {
        players.append(lineEdit->text());
    }
    emit playersChanged(players);
}
